// thêm số
var numArr = [];

function themSo() {
  var number = document.querySelector('#number').value * 1;

  numArr.push(number);
  console.log('numArr: ', numArr);
  document.querySelector('.result').innerHTML = numArr;
}
// 1. tổng số dương
function tongSoDuong() {
  var tongSoDuong = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      tongSoDuong += numArr[i];
    }
  }
  document.querySelector('.result1').innerHTML = tongSoDuong;
}
// 2. đếm số dương
function demSoDuong() {
  var demSoDuong = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      demSoDuong++;
    }
  }
  document.querySelector('.result2').innerHTML = `Có ${demSoDuong} số dương`;
}
// 3. tìm số nhỏ nhất
//cách 1

/*
function timSoNhoNhat() {
  var soNhoNhat = numArr[0];
  for (var i = 1; i < numArr.length; i++) {
    if (numArr[i] < soNhoNhat) {
      soNhoNhat = numArr[i];
    }
  }
  document.querySelector(
    '.result3'
  ).innerHTML = ` số dương nhỏ nhất là: ${soNhoNhat}`;
}
*/

//cách 2
function timSoNhoNhat() {
  for (var i = 1; i < numArr.length; i++) {
    if (
      numArr.sort(function (a, b) {
        return a - b;
      })
    ) {
      var soNhoNhat = numArr[0];
    }
  }
  document.querySelector(
    '.result3'
  ).innerHTML = ` số nhỏ nhất là: ${soNhoNhat}`;
}

// 4. tìm số dương nhỏ nhất

newArr = [];

function timSoDuongNhoNhat() {
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      newArr.push(numArr[i]);
    }
  }
  //
  var soDuongNhoNhat = newArr[0];
  for (var i = 1; i < newArr.length; i++) {
    if (soDuongNhoNhat > newArr[i]) {
      soDuongNhoNhat = newArr[i];
    }
    console.log('newArr2: ', newArr);
  }
  document.querySelector(
    '.result4'
  ).innerHTML = ` số dương nhỏ nhất là: ${soDuongNhoNhat}`;
}
// 5. Đổi chỗ
function doiCho() {
  var position1 = document.querySelector('#n1').value;
  var position2 = document.querySelector('#n2').value;
  var temp = numArr[position1];
  numArr[position1] = numArr[position2];
  numArr[position2] = temp;
  document.querySelector(
    '.result5'
  ).innerHTML = ` Mảng sau khi đổi chổ là: ${numArr}`;
}
// 6. tìm số chẵn cuối cùng
soChanArr = [];

function timSoChanCuoiCung() {
  // for (var i = 0; i < numArr.length; i++) {
  //   if (numArr[i] % 2 === 0) {
  //     soChanArr.push(numArr[i]);
  //   }
  // }
  // //
  // console.log('soChanArr3: ', soChanArr);

  for (var i = 1; i < numArr.length; i++) {
    var soChanCuoiCung = 0;

    if (numArr[i] % 2 == 0) {
      soChanCuoiCung = numArr[i];

      document.querySelector(
        '.result6'
      ).innerHTML = ` số chẵn cuối cùng là: ${soChanCuoiCung}`;
    }
  }
}
// 7. sắp xếp tăng dần
function sapXepTangDan() {
  numArr.sort(function (a, b) {
    return a - b;
  });
  document.querySelector(
    '.result7'
  ).innerHTML = `sắp xếp mảng tăng dần: ${numArr}`;
}
// 8. tìm số nguyênn tố đầu tiên
// check số nguyên tố

var isSoNguyenTo = function (num) {
  var soNguyenTo = true;
  if (num < 2) {
    soNguyenTo = false;
  } else if (num == 2) {
    soNguyenTo = true;
  } else if (num % 2 == 0) {
    soNguyenTo = false;
  } else {
    for (var i = 3; i < num - 1; i += 2) {
      if (num % i == 0) {
        soNguyenTo = false;
        break;
      }
    }
  }
  return soNguyenTo;
};

// check số nguyên tố 2

function kiemTraPrimeNumber(n) {
  var isprime = n == 1 ? false : true; //because 1 is not prime
  for (var i = 2; i < n; i++) {
    n % i == 0 ? (isprime *= false) : (isprime *= true);
  }
  return isprime;
}
// tìm số nguyên tố đầu tiên
function timSoNguyenToDauTien() {
  var soNguyenToDauTien;
  for (var i = 0; i < numArr.length; i++) {
    if (isSoNguyenTo(numArr[i])) {
      soNguyenToDauTien = numArr[i];
      break;
    }
  }
  document.querySelector(
    '.result8'
  ).innerHTML = `số nguyên tố đầu tiên là: ${soNguyenToDauTien}`;
}
// 9. đếm số nguyên
function demSoNguyen() {
  var demSoNguyen = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (Number.isInteger(numArr[i])) {
      demSoNguyen++;
    }
  }
  document.querySelector(
    '.result9'
  ).innerHTML = `có tât cả ${demSoNguyen} số nguyên`;
}

// 10. So sánh số lượng số âm và dương

function soSanhSoLuongSoAmVaDuong() {
  var soLuongSoDuong = 0;
  var soLuongSoAm = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      soLuongSoDuong++;
    } else {
      soLuongSoAm++;
    }
  }
  if (soLuongSoAm > soLuongSoDuong) {
    document.querySelector(
      '.result10'
    ).innerHTML = `có ${soLuongSoDuong} số dương, và ${soLuongSoAm} số âm, nên số lượng số âm > số lượng số dương`;
  } else if (soLuongSoAm == soLuongSoDuong) {
    document.querySelector(
      '.result10'
    ).innerHTML = `có ${soLuongSoDuong} số dương, và ${soLuongSoAm} số âm, nên số lượng số âm = số lượng số dương`;
  } else {
    document.querySelector(
      '.result10'
    ).innerHTML = `có ${soLuongSoDuong} số dương, và ${soLuongSoAm} số âm, nên số lượng số dương > số lượng số âm`;
  }
}
